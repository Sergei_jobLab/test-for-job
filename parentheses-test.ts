type IIsCorrectString = (input:string) => string

const isCorrectString = (input) => {
    const openParentheses = ["{", "[", "("]
    const closeParentheses = ["}", "]", ")"]
  
    let stack:Array<string> = []
  
    let correct = 0
    let incorrect = 0
  
    input.split("").forEach((element) => {
      if (
        openParentheses.includes(stack[stack.length - 1]) &&
        openParentheses.indexOf(stack[stack.length - 1]) ===
          closeParentheses.indexOf(element) &&
        closeParentheses.includes(element)
      ) {
        stack.pop()
      } else {
        stack.push(element)
      }
    })
  
    return stack.length === 0
      ? "Все скобки расставлены корректно"
      : `Не корректно:${stack.length}, корректно:${input.length - stack.length}`
  }
  
  console.log(isCorrectString("()"))
  console.log(isCorrectString("(){}"))
  console.log(isCorrectString("(()[{}])"))
  console.log(isCorrectString("()}{}"))
  console.log(isCorrectString("()}()}"))