import { useState, useEffect } from "react"

export const useCards = () => {
  const [cards, setCards] = useState([
    { timer: 5, index: 1, initTimer: 5 },
  ])

  // каждую секунду уменьшает таймер у всех елементов на 1
  useEffect(() => {
    const interval = setInterval(() => {
      setCards((prevCards) =>
        prevCards.map(({ index, timer, initTimer }) => ({
          index,
          initTimer,
          timer: timer - 1,
        }))
      )
    }, 1000)
    return () => clearInterval(interval) 
  }, [])

  useEffect(() => {
    cards.forEach((card) => {
      if (card.timer < 1) {
        //Удаляет item с просроченным таймером
        setCards((prevCards) =>
          prevCards.filter(
            (cardForFilter) => cardForFilter.index !== card.index
          )
        )
        //Уменьшает таймер у всех карточек при удалении одной
        setCards((prevCards) =>
          prevCards.map((c) => ({ ...c, timer: c.timer - card.initTimer }))
        )
      }
    })
  }, [cards])

  return { cards, setCards }
}
