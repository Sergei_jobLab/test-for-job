import { Layout, Divider, List, Button } from "antd"
import { FC } from "react"
import { Item } from "./Item"
import { RandomInteger } from "Utils/randomInteger"
import { useCards } from "Hooks/useCards"

export const App: FC = () => {
  const { cards, setCards } = useCards()

  const { Content, Footer, Header } = Layout

  const handlerAdd = () => {
    const randomTimer = RandomInteger(10, 31)
    setCards((prevCards) => [
      ...prevCards,
      {
        index: cards.length>0 ? cards[cards.length - 1].index + 1 : 1,
        timer: randomTimer,
        initTimer: randomTimer,
      },
    ])
  }

  return (
    <Layout className="layout" style={{ minHeight: "100vh" }}>
      <Header></Header>
      <Content style={{ padding: "50px" }}>
        <Divider orientation="left">Items</Divider>
        <div style={{ maxHeight: "400px", overflow: "auto" }}>
          <List
            bordered
            dataSource={cards}
            renderItem={(item) => <Item {...item} />}
          />
        </div>
        <Button
          onClick={handlerAdd}
          type="primary"
          style={{ marginTop: "50px" }}
        >
          add item
        </Button>
      </Content>
      <Footer>Items Test ©2022 Sergei K</Footer>
    </Layout>
  )
}
