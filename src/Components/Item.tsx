import { Button, List } from "antd"
import { FC, memo } from "react"

interface IItem {
  timer: number
  index: number
}

export const Item: FC<IItem> = memo(({ index, timer }) => {
  return (
    <List.Item style={{ justifyContent: "start", gap: "20px" }}>
      <Button type="primary">{index}</Button>
      {timer}
    </List.Item>
  )
})
